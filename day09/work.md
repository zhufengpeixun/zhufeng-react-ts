## 13.课程详情

文件目录

```js
.
├── package.json
├── src
│   ├── api
│   │   ├── home.tsx
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── LessonList
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   ├── HomeHeader
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   ├── HomeSliders
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   └── LessonList
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Login
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   ├── Profile
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Register
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mine.tsx
│   │       └── profile.tsx
│   ├── typings
│   │   ├── images.d.ts
│   │   ├── lesson.tsx
│   │   ├── login-types.tsx
│   │   ├── slider.tsx
│   │   └── user.tsx
│   └── utils.tsx
├── tsconfig.json
└── webpack.config.js
```

页面效果
![lessondetail](https://img.zhufengpeixun.com/lessondetail.gif)

### 13.1 src\index.tsx

src\index.tsx

```diff
import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Redirect } from "react-router-dom"; //三个路由组件
import { Provider } from "react-redux"; //负责把属性中的store传递给子组件
import store from "./store"; //引入仓库
import { ConfigProvider } from "antd"; //配置
import zh_CN from "antd/lib/locale-provider/zh_CN"; //国际化中文
import "./assets/css/common.less"; //通用的样式
import Tabs from "./components/Tabs"; //引入底部的页签导航
import Home from "./routes/Home"; //首页
import Mine from "./routes/Mine"; //我的课程
import Profile from "./routes/Profile"; //个人中心
import Register from "./routes/Register";
import Login from "./routes/Login";
+import Detail from "./routes/Detail";
import { ConnectedRouter } from "connected-react-router"; //redux绑定路由
import history from "./store/history";
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <ConfigProvider locale={zh_CN}>
        <main className="main-container">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/mine" component={Mine} />
            <Route path="/profile" component={Profile} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
+           <Route path="/detail/:id" component={Detail} />
            <Redirect to="/" />
          </Switch>
        </main>
        <Tabs />
      </ConfigProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root")
);
```

### 13.2 src\api\home.tsx

src\api\home.tsx

```diff
import axios from "./index";
export function getSliders() {
  return axios.get("/slider/list");
}
export function getLessons(
  currentCategory: string = "all",
  offset: number,
  limit: number
) {
  return axios.get(
    `/lesson/list?category=${currentCategory}&offset=${offset}&limit=${limit}`
  );
}
+export function getLesson<T>(id: string) {
+  return axios.get<T, T>(`/lesson/${id}`);
+}

```

### 13.3 routes\Detail\index.tsx

src\routes\Detail\index.tsx

```js
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Card, Button } from "antd";
import NavHeader from "@/components/NavHeader";
import { getLesson } from "@/api/home";
import { RouteComponentProps } from "react-router";
import Lesson from "@/typings/lesson";
import { StaticContext } from "react-router";
import { LessonResult } from "@/typings/lesson";
const { Meta } = Card;
interface Params {
  id: string;
}
type RouteProps = RouteComponentProps<Params, StaticContext, Lesson>;
type Props = RouteProps & {
  children?: any;
};

function Detail(props: Props) {
  let [lesson, setLesson] = useState<Lesson>({} as Lesson);
  useEffect(() => {
    (async () => {
      let lesson: Lesson = props.location.state;
      if (!lesson) {
        let id = props.match.params.id;
        let result: LessonResult = await getLesson<LessonResult>(id);
        if (result.success) lesson = result.data;
      }
      setLesson(lesson);
    })();
  }, []);
  return (
    <>
      <NavHeader history={props.history}>课程详情</NavHeader>
      <Card
        hoverable
        style={{ width: "100%" }}
        cover={<video src={lesson.video} controls autoPlay={false} />}
      >
        <Meta title={lesson.title} description={<p>价格: {lesson.price}</p>} />
      </Card>
    </>
  );
}

export default connect()(Detail);

```

## 14.购物车

- 本章我们学习购物车

目录结构

```js
.
├── package.json
├── src
│   ├── api
│   │   ├── home.tsx
│   │   ├── index.tsx
│   │   └── profile.tsx
│   ├── assets
│   │   ├── css
│   │   │   └── common.less
│   │   └── images
│   │       └── logo.png
│   ├── components
│   │   ├── LessonList
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── NavHeader
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Tabs
│   │       ├── index.less
│   │       └── index.tsx
│   ├── index.html
│   ├── index.tsx
│   ├── routes
│   │   ├── Home
│   │   │   ├── components
│   │   │   │   ├── HomeHeader
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   ├── HomeSliders
│   │   │   │   │   ├── index.less
│   │   │   │   │   └── index.tsx
│   │   │   │   └── LessonList
│   │   │   │       ├── index.less
│   │   │   │       └── index.tsx
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Login
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   ├── Mine
│   │   │   └── index.tsx
│   │   ├── Profile
│   │   │   ├── index.less
│   │   │   └── index.tsx
│   │   └── Register
│   │       ├── index.less
│   │       └── index.tsx
│   ├── store
│   │   ├── actions
│   │   │   ├── home.tsx
│   │   │   └── profile.tsx
│   │   ├── action-types.tsx
│   │   ├── history.tsx
│   │   ├── index.tsx
│   │   └── reducers
│   │       ├── home.tsx
│   │       ├── index.tsx
│   │       ├── mine.tsx
│   │       └── profile.tsx
│   ├── typings
│   │   ├── images.d.ts
│   │   ├── lesson.tsx
│   │   ├── login-types.tsx
│   │   ├── slider.tsx
│   │   └── user.tsx
│   └── utils.tsx
├── tsconfig.json
└── webpack.config.js
```

页面效果

![cartitems](https://img.zhufengpeixun.com/cartitems.gif)

### 14.1 src\index.tsx

src\index.tsx

```diff
import React from "react";
import ReactDOM from "react-dom";
import { Switch, Route, Redirect } from "react-router-dom"; //三个路由组件
import { Provider } from "react-redux"; //负责把属性中的store传递给子组件
import { store, persistor } from "./store"; //引入仓库
import { ConfigProvider } from "antd"; //配置
import zh_CN from "antd/lib/locale-provider/zh_CN"; //国际化中文
import "./assets/css/common.less"; //通用的样式
import Tabs from "./components/Tabs"; //引入底部的页签导航
import Home from "./routes/Home"; //首页
import Mine from "./routes/Mine"; //我的课程
import Profile from "./routes/Profile"; //个人中心
import Register from "./routes/Register";
import Login from "./routes/Login";
import Detail from "./routes/Detail";
+import Cart from "./routes/Cart";
import { ConnectedRouter } from "connected-react-router"; //redux绑定路由
+import { PersistGate } from "redux-persist/integration/react";
import history from "./store/history";
ReactDOM.render(
  <Provider store={store}>
+    <PersistGate loading={null} persistor={persistor}>
      <ConnectedRouter history={history}>
        <ConfigProvider locale={zh_CN}>
          <main className="main-container">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/mine" component={Mine} />
              <Route path="/profile" component={Profile} />
              <Route path="/register" component={Register} />
              <Route path="/login" component={Login} />
              <Route path="/detail/:id" component={Detail} />
+              <Route path="/cart" component={Cart} />
              <Redirect to="/" />
            </Switch>
          </main>
          <Tabs />
        </ConfigProvider>
      </ConnectedRouter>
+    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

```

### 14.2 src\routes\Detail\index.tsx

src\routes\Detail\index.tsx

```diff
import React, { useState, useEffect, PropsWithChildren } from "react";
import { connect } from "react-redux";
import { Card, Button } from "antd";
import NavHeader from "@/components/NavHeader";
import { getLesson } from "@/api/home";
import { RouteComponentProps } from "react-router";
import Lesson from "@/typings/lesson";
import { StaticContext } from "react-router";
import { LessonResult } from "@/typings/lesson";
import actions from "@/store/actions/cart";
import { CombinedState } from "@/store/reducers";
import "./index.less";
const { Meta } = Card;
interface Params {
  id: string;
}
+type RouteProps = RouteComponentProps<Params, StaticContext, Lesson>;
+type StateProps = ReturnType<typeof mapStateToProps>;
+type DispatchProps = typeof actions;
+type Props = PropsWithChildren<RouteProps & StateProps & DispatchProps>;

function Detail(props: Props) {
  let [lesson, setLesson] = useState<Lesson>({} as Lesson);
  useEffect(() => {
    (async () => {
      let lesson: Lesson = props.location.state;
      if (!lesson) {
        let id = props.match.params.id;
        let result: LessonResult = await getLesson<LessonResult>(id);
        if (result.success) lesson = result.data;
      }
      setLesson(lesson);
    })();
  }, []);
+  const addCartItem = (lesson: Lesson) => {
+    //https://developer.mozilla.org/zh-CN/docs/Web/API/Element/getBoundingClientRect
+    let video: HTMLVideoElement = document.querySelector("#lesson-video");

+    let cart: HTMLSpanElement = document.querySelector(
+      ".anticon.anticon-shopping-cart"
+    );
+    let clonedVideo: HTMLVideoElement = video.cloneNode(
+      true
+    ) as HTMLVideoElement;
+    let videoWith = video.offsetWidth;
+    let videoHeight = video.offsetHeight;
+    let cartWith = cart.offsetWidth;
+    let cartHeight = cart.offsetHeight;
+    let videoLeft = video.getBoundingClientRect().left;
+    let videoTop = video.getBoundingClientRect().top;
+    let cartRight = cart.getBoundingClientRect().right;
+    let cartBottom = cart.getBoundingClientRect().bottom;
+    clonedVideo.style.cssText = `
+          z-index: 1000;
+          opacity:0.8;
+          position:fixed;
+          width:${videoWith}px;
+          height:${videoHeight}px;
+          top:${videoTop}px;
+          left:${videoLeft}px;
+          transition: all 2s ease-in-out;
+        `;
+    document.body.appendChild(clonedVideo);
+    setTimeout(function () {
+      clonedVideo.style.left = cartRight - cartWith / 2 + "px";
+      clonedVideo.style.top = cartBottom - cartHeight / 2 + "px";
+      clonedVideo.style.width = `0px`;
+      clonedVideo.style.height = `0px`;
+      clonedVideo.style.opacity = "50";
+    }, 0);
+    props.addCartItem(lesson);
+  };
  return (
    <>
      <NavHeader history={props.history}>课程详情</NavHeader>
+      <Card
+        hoverable
+        style={{ width: "100%" }}
+        cover={<img id="lesson-video" src={lesson.poster} />}
+      >
+        <Meta
+          title={lesson.title}
+          description={
+            <>
+              <p>价格: ¥{lesson.price}元</p>
+              <p>
+                <Button
+                  className="add-cart"
+                  onClick={() => addCartItem(lesson)}
+                >
+                  加入购物车
+                </Button>
+              </p>
+            </>
+          }
+        />
+      </Card>
    </>
  );
}
/**
 * https://cubic-bezier.com/#0,0,1,1
 * linear:cubic-bezier(0,0,1,1)             匀速运动
 * ease:cubic-bezier(0.25,0.1,0.25,1)       先慢后快再慢
 * ease-in:cubic-bezier(0.42,0,1,1)         先慢后快
 * ease-out:cubic-bezier(0,0,0.58,1)        先快后慢
 * ease-in-out:cubic-bezier(0.42,0,0.58,1)  先慢后快再慢
 */
+let mapStateToProps = (state: CombinedState): CombinedState => state;
+export default connect(mapStateToProps, actions)(Detail);

```

src\routes\Detail\index.less

```less
button.add-cart {
  &:hover {
    background-color: #f71f40;
    color: #fff;
  }
}
```

### 14.3 action-types.tsx

src\store\action-types.tsx

```diff
export const ADD = "ADD";
//设置当前分类的名称
export const SET_CURRENT_CATEGORY = "SET_CURRENT_CATEGORY";
//发起验证用户是否登录的请求
export const VALIDATE = "VALIDATE";

export const LOGOUT = "LOGOUT";
//上传头像
export const CHANGE_AVATAR = "CHANGE_AVATAR";
export const GET_SLIDERS = "GET_SLIDERS";

export const GET_LESSONS = "GET_LESSONS";
export const SET_LESSONS_LOADING = "SET_LESSONS_LOADING";
export const SET_LESSONS = "SET_LESSONS";
export const REFRESH_LESSONS = "REFRESH_LESSONS";

+export const ADD_CART_ITEM = "ADD_CART_ITEM"; //向购物车中增一个商品
+export const REMOVE_CART_ITEM = "REMOVE_CART_ITEM"; //从购物车中删除一个商品
+export const CLEAR_CART_ITEMS = "CLEAR_CART_ITEMS"; //清空购物车
+export const CHANGE_CART_ITEM_COUNT = "CHANGE_CART_ITEM_COUNT"; //直接修改购物车商品的数量减1
+export const CHANGE_CHECKED_CART_ITEMS = "CHANGE_CHECKED_CART_ITEMS"; //选中商品
+export const SETTLE = "SETTLE"; //结算

```

### 14.4 src\store\index.tsx

src\store\index.tsx

```diff
import {
   createStore,
   applyMiddleware,
   Store,
+  AnyAction,
+  Dispatch,
} from "redux";
import reducers, { CombinedState } from "./reducers";
import logger from "redux-logger";
import thunk, { ThunkDispatch } from "redux-thunk";
import promise from "redux-promise";
import { routerMiddleware } from "connected-react-router";
+import { persistStore, persistReducer } from "redux-persist";
+import storage from "redux-persist/lib/storage";
+import history from "./history";
+const persistConfig = {
+  key: "root",
+  storage,
+  whitelist: ["cart"],
+};
+const persistedReducer = persistReducer(persistConfig, reducers);
+let store: Store<CombinedState, AnyAction> = createStore<
+  CombinedState,
+  AnyAction,
+  {},
+  {}
+>(
+  persistedReducer,
+  applyMiddleware(thunk, routerMiddleware(history), promise, logger)
+);
+let persistor = persistStore(store);
+export type StoreGetState = () => CombinedState;
+export type StoreDispatch = Dispatch &
+  ThunkDispatch<CombinedState, any, AnyAction>;
+export { store, persistor };

```

### 14.4 actions\home.tsx

src\store\actions\home.tsx

```diff
import * as TYPES from "../action-types";
import { getSliders, getLessons } from "@/api/home";
+import { StoreGetState, StoreDispatch } from "../index";
export default {
  setCurrentCategory(currentCategory: string) {
    return { type: TYPES.SET_CURRENT_CATEGORY, payload: currentCategory };
  },
  getSliders() {
    return {
      type: TYPES.GET_SLIDERS,
      payload: getSliders(),
    };
  },
  getLessons() {
+    return (dispatch: StoreDispatch, getState: StoreGetState) => {
      (async function () {
        let {
          currentCategory,
          lessons: { hasMore, offset, limit, loading },
        } = getState().home;
        if (hasMore && !loading) {
          dispatch({ type: TYPES.SET_LESSONS_LOADING, payload: true });
          let result = await getLessons(currentCategory, offset, limit);
          dispatch({ type: TYPES.SET_LESSONS, payload: result.data });
        }
      })();
    };
  },
  refreshLessons() {
+    return (dispatch: StoreDispatch, getState: StoreGetState) => {
      (async function () {
        let {
          currentCategory,
          lessons: { limit, loading },
        } = getState().home;
        if (!loading) {
          dispatch({ type: TYPES.SET_LESSONS_LOADING, payload: true });
          let result = await getLessons(currentCategory, 0, limit);
          dispatch({ type: TYPES.REFRESH_LESSONS, payload: result.data });
        }
      })();
    };
  },
};

```

### 14.5 reducers\index.tsx

src\store\reducers\index.tsx

```diff
import { ReducersMapObject, Reducer } from "redux";
import { connectRouter } from "connected-react-router";
import history from "../history";
import home from "./home";
import mine from "./mine";
+import cart from "./cart";
+import { combineReducers } from "redux-immer";
+import produce from "immer";
import profile from "./profile";
let reducers: ReducersMapObject = {
  router: connectRouter(history),
  home,
  mine,
  profile,
+ cart,
};
type CombinedState = {
  [key in keyof typeof reducers]: ReturnType<typeof reducers[key]>;
};
+let reducer: Reducer<CombinedState> = combineReducers<CombinedState>(
+  produce,
+  reducers
+);
export { CombinedState };
export default reducer;
```

### 14.6 typings\cart.tsx

src\typings\cart.tsx

```js
import Lesson from "./lesson";
export interface CartItem {
  lesson: Lesson;
  count: number;
  checked: boolean;
}
export type CartState = CartItem[];
```

### 14.7 reducers\cart.tsx

src\store\reducers\cart.tsx

```js
import { AnyAction } from "redux";
import { CartState } from "@/typings/cart";
import * as actionTypes from "@/store/action-types";
let initialState: CartState = [];
export default function (
  state: CartState = initialState,
  action: AnyAction
): CartState {
  switch (action.type) {
    case actionTypes.ADD_CART_ITEM:
      let oldIndex = state.findIndex(
        (item) => item.lesson.id === action.payload.id
      );
      if (oldIndex == -1) {
        return [
          ...state,
          {
            checked: false,
            count: 1,
            lesson: action.payload,
          },
        ];
      } else {
        let lesson = state[oldIndex];
        return [
          ...state.slice(0, oldIndex),
          { ...lesson, count: lesson.count + 1 },
          ...state.slice(oldIndex + 1),
        ];
      }
    case actionTypes.REMOVE_CART_ITEM:
      let removeIndex = state.findIndex(
        (item) => item.lesson.id === action.payload
      );
      return [...state.slice(0, removeIndex), ...state.slice(removeIndex + 1)];
    case actionTypes.CLEAR_CART_ITEMS:
      return [];
    case actionTypes.CHANGE_CART_ITEM_COUNT:
      return state.map((item) => {
        if (item.lesson.id === action.payload.id) {
          item.count = action.payload.count;
        }
        return item;
      });
    case actionTypes.CHANGE_CHECKED_CART_ITEMS:
      let checkedIds = action.payload;
      return state.map((item) => {
        if (checkedIds.includes(item.lesson.id)) {
          item.checked = true;
        } else {
          item.checked = false;
        }
        return item;
      });
    case actionTypes.SETTLE:
      return state.filter((item) => !item.checked);
    default:
      return state;
  }
}
```

### 14.8 reducers\home.tsx

src\store\reducers\home.tsx

```diff
import { AnyAction } from "redux";
import * as TYPES from "../action-types";
import Slider from "@/typings/slider";
import Lesson from "@/typings/Lesson";

export interface Lessons {
  loading: boolean;
  list: Lesson[];
  hasMore: boolean;
  offset: number;
  limit: number;
}
export interface HomeState {
  currentCategory: string;
  sliders: Slider[];
  lessons: Lessons;
}
let initialState: HomeState = {
  currentCategory: "all", //默认当前的分类是显示全部类型的课程
  sliders: [],
  lessons: {
    loading: false,
    list: [],
    hasMore: true,
    offset: 0,
    limit: 5,
  },
};
export default function (
  state: HomeState = initialState,
  action: AnyAction
): HomeState {
  switch (action.type) {
    case TYPES.SET_CURRENT_CATEGORY: //修改当前分类
      return { ...state, currentCategory: action.payload };
    case TYPES.GET_SLIDERS:
      return { ...state, sliders: action.payload.data };
    case TYPES.SET_LESSONS_LOADING:
-        return {
-        ...state,
-        lessons: { ...state.lessons, loading: action.payload },
-      };
+      state.lessons.loading = action.payload;
+      return state;
    case TYPES.SET_LESSONS:
-       return {
-        ...state,
-        lessons: {
-          ...state.lessons,
-          loading: false,
-          hasMore: action.payload.hasMore,
-          list: [...state.lessons.list, ...action.payload.list],
-          offset: state.lessons.offset + action.payload.list.length,
-        },
-      };
+      state.lessons.loading = false;
+      state.lessons.hasMore = action.payload.hasMore;
+      state.lessons.list = [...state.lessons.list, ...action.payload.list];
+      state.lessons.offset = state.lessons.offset + action.payload.list.length;
+      return state;
    case TYPES.REFRESH_LESSONS:
-        return {
-        ...state,
-        lessons: {
-          ...state.lessons,
-          loading: false,
-          hasMore: action.payload.hasMore,
-          list: action.payload.list,
-          offset: action.payload.list.length,
-        },
-      };
-      state.lessons.loading = false;
+      state.lessons.hasMore = action.payload.hasMore;
+      state.lessons.list = action.payload.list;
+      state.lessons.offset = action.payload.list.length;
+      return state;
    default:
      return state;
  }
}
```

### 14.9 actions\cart.tsx

src\store\actions\cart.tsx

```js
import * as actionTypes from "../action-types";
import Lesson from "@/typings/lesson";
import { message } from "antd";
import { push } from "connected-react-router";
import { StoreGetState, StoreDispatch } from "../index";
export default {
  addCartItem(lesson: Lesson) {
    return function (dispatch: StoreDispatch) {
      dispatch({
        type: actionTypes.ADD_CART_ITEM,
        payload: lesson,
      });
      message.info("添加课程成功");
    };
  },
  removeCartItem(id: string) {
    return {
      type: actionTypes.REMOVE_CART_ITEM,
      payload: id,
    };
  },
  clearCartItems() {
    return {
      type: actionTypes.CLEAR_CART_ITEMS,
    };
  },
  changeCartItemCount(id: string, count: number) {
    return {
      type: actionTypes.CHANGE_CART_ITEM_COUNT,
      payload: {
        id,
        count,
      },
    };
  },
  changeCheckedCartItems(checkedIds: string[]) {
    return {
      type: actionTypes.CHANGE_CHECKED_CART_ITEMS,
      payload: checkedIds,
    };
  },
  settle() {
    return function (dispatch: StoreDispatch, getState: StoreGetState) {
      dispatch({
        type: actionTypes.SETTLE,
      });
      dispatch(push("/"));
    };
  },
};
```

### 14.10 Cart\index.tsx

src\routes\Cart\index.tsx

```js
import React, { PropsWithChildren, useState } from "react";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import {
  Table,
  Button,
  InputNumber,
  Popconfirm,
  Row,
  Col,
  Badge,
  Modal,
} from "antd";
import { CombinedState } from "@/store/reducers";
import NavHeader from "@/components/NavHeader";
import Lesson from "@/typings/lesson";
import { StaticContext } from "react-router";
import actions from "@/store/actions/cart";
import { CartItem } from "@/typings/cart";
import { getNumber } from "@/utils";
interface Params {
  id: string;
}
type RouteProps = RouteComponentProps<Params, StaticContext, Lesson>;
interface Params {
  id: string;
}
type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof actions;
type Props = PropsWithChildren<RouteProps & StateProps & DispatchProps>;
function Cart(props: Props) {
  let [settleVisible, setSettleVisible] = useState(false);
  const confirmSettle = () => {
    setSettleVisible(true);
  };
  const handleOk = () => {
    setSettleVisible(false);
    props.settle();
  };
  const handleCancel = () => {
    setSettleVisible(false);
  };
  const columns = [
    {
      title: "商品",
      dataIndex: "lesson",
      render: (val: Lesson, row: CartItem) => (
        <>
          <p>{val.title}</p>
          <p>单价:{val.price}</p>
        </>
      ),
    },
    {
      title: "数量",
      dataIndex: "count",
      render: (val: number, row: CartItem) => (
        <InputNumber
          size="small"
          min={1}
          max={10}
          value={val}
          onChange={(value: any) =>
            props.changeCartItemCount(row.lesson.id, value)
          }
        />
      ),
    },
    {
      title: "操作",
      render: (val: any, row: CartItem) => (
        <Popconfirm
          title="是否要删除商品?"
          onConfirm={() => props.removeCartItem(row.lesson.id)}
          okText="是"
          cancelText="否"
        >
          <Button size="small" type="primary">
            删除
          </Button>
        </Popconfirm>
      ),
    },
  ];
  const rowSelection = {
    selectedRowKeys: props.cart
      .filter((item: CartItem) => item.checked)
      .map((item: CartItem) => item.lesson.id),
    onChange: (selectedRowKeys: string[]) => {
      props.changeCheckedCartItems(selectedRowKeys);
    },
  };
  let totalCount: number = props.cart
    .filter((item: CartItem) => item.checked)
    .reduce((total: number, item: CartItem) => total + item.count, 0);
  let totalPrice = props.cart
    .filter((item: CartItem) => item.checked)
    .reduce(
      (total: number, item: CartItem) =>
        total + getNumber(item.lesson.price) * item.count,
      0
    );
  return (
    <>
      <NavHeader history={props.history}>购物车</NavHeader>
      <Table
        rowKey={(row) => row.lesson.id}
        rowSelection={rowSelection}
        columns={columns}
        dataSource={props.cart}
        pagination={false}
        size="small"
      />
      <Row style={{ padding: "5px" }}>
        <Col span={4}>
          <Button type="primary" size="small" onClick={props.clearCartItems}>
            清空
          </Button>
        </Col>
        <Col span={9}>
          已经选择{totalCount > 0 ? <Badge count={totalCount} /> : 0}件商品
        </Col>
        <Col span={7}>总价: ¥{totalPrice}元</Col>
        <Col span={4}>
          <Button type="primary" size="small" onClick={confirmSettle}>
            去结算
          </Button>
        </Col>
      </Row>
      <Modal
        title="去结算"
        visible={settleVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>请问你是否要结算?</p>
      </Modal>
    </>
  );
}
let mapStateToProps = (state: CombinedState): CombinedState => state;
export default connect(mapStateToProps, actions)(Cart);
```
视频：[购物车](https://img.zhufengpeixun.com/10.%E8%B4%AD%E7%89%A9%E8%BD%A6.mp4)
[课程列表优化](https://img.zhufengpeixun.com/11.%E8%AF%BE%E7%A8%8B%E5%88%97%E8%A1%A8%E4%BC%98%E5%8C%96.mp4)
[购物车动画](https://img.zhufengpeixun.com/12.%E8%B4%AD%E7%89%A9%E8%BD%A6%E5%8A%A8%E7%94%BB.mp4)